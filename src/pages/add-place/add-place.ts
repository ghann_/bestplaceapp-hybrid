import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ModalController, AlertController } from "ionic-angular";
import { SetLocationPage } from "../set-location/set-location";
import { Geolocation } from "@ionic-native/geolocation";
import { Camera, CameraOptions } from '@ionic-native/camera';

import { Loc } from "../../models/location";
import { PlacePage } from '../place/place';

@Component({
  selector: 'page-add-place',
  templateUrl: 'add-place.html',
})
export class AddPlacePage {
  gotLocation = false;
  gotPicture = false;
  imageUrl = '';
  marker: Loc;
  lat = 0;
  lng = 0;
  picTaken: Date;

  constructor(private modalCtrl: ModalController, private geoloc: Geolocation, private camera: Camera, private alertCtrl: AlertController){
  }
  
  onSubmit(f: NgForm) {
    
    if(!this.gotLocation)this.presentAlert("Error: Location is missing"); else
    if(!this.gotPicture) this.presentAlert("Error: Photo is missing"); 
    else{
      let placeData = {
        title: f.value.title,
        desc: f.value.description,
        dateOfPicTaken: this.picTaken,
        imageURL: this.imageUrl,
        lat: this.lat,
        lng: this.lng
      }
      const modal = this.modalCtrl.create(PlacePage, {data: placeData});
      modal.present();
    }
  }

  presentAlert(message:string) {
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: message,
      buttons: ['Ok']
    });
    alert.present();
  }

  onOpenMap() {
    const modal = this.modalCtrl.create(SetLocationPage);
    modal.present();
    modal.onDidDismiss((data) => {
      console.log(data)
      if(data.canceled == true){
       return
      }else
      if(data.canceled == null){
        this.lat = data.lat;
        this.lng = data.lng;
        this.marker = new Loc(data.lat, data.lng);
        this.gotLocation = true;
      }
    });
  }

  onSetMarker(event: any) {
    this.marker = new Loc(event.coords.lat, event.coords.lng);
  }

  onLocate() {
    this.geoloc.getCurrentPosition()
      .then(
        currLocation => {
          this.lat = currLocation.coords.latitude;
          this.lng = currLocation.coords.longitude;
          this.marker = new Loc(this.lat, this.lng);
          this.gotLocation = true;
        }
      )
      .catch(
        error => {
          console.log(error);
        }
      )
  }

  onTakePhoto() {
    const options: CameraOptions = {
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      targetWidth: 1000,
      targetHeight: 1000
    }

    this.camera.getPicture(options).then(
        (imageData) => {
          this.imageUrl = "data:image/jpeg;base64," + imageData;
          this.gotPicture = true;
          this.picTaken = new Date();
        }, 
        (err) => {
          console.log(err);
        });
  }
}
