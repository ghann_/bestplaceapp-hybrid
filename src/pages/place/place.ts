import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ViewController } from 'ionic-angular/navigation/view-controller';

@IonicPage()
@Component({
  selector: 'page-place',
  templateUrl: 'place.html',
})
export class PlacePage {
  title = ""
  desc = ""
  dateOfPicTaken: Date;
  imageURL: any
  lat = 0
  lng = 0

  constructor(public viewCtrl:ViewController, public navParams: NavParams) {
    let data = navParams.get('data')
    this.title = data.title
    this.desc = data.desc
    this.dateOfPicTaken = data.dateOfPicTaken
    this.imageURL = data.imageURL
    this.lat = data.lat.toFixed(7)
    this.lng = data.lng.toFixed(7)
  }
  
  closeModal(){
    this.viewCtrl.dismiss();
  }
}
