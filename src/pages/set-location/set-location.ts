import { Component } from '@angular/core';
import { ViewController } from "ionic-angular";

import { Loc } from '../../models/location'

@Component({
  selector: 'page-set-location',
  templateUrl: 'set-location.html',
})
export class SetLocationPage {
  marker: Loc;
  lat = -6.178306;
  lng = 106.631889;
  canceled = true;

  constructor(private viewCtrl: ViewController) {
    this.marker = new Loc(this.lat, this.lng);
  }
  
  onClose() {
    this.viewCtrl.dismiss(this.canceled);
  }
  onSetMarker(event: any) {
    this.marker = new Loc(event.coords.lat, event.coords.lng);
    this.canceled = false;
  }
  onConfirm(){
    this.viewCtrl.dismiss(this.marker);
  }
}
